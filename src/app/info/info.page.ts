import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { DataSvcModule } from '../data-svc/data-svc.module';
import { AccuraScanRes } from '../data-svc/interfaces';

@Component({
  selector: 'app-info',
  templateUrl: './info.page.html',
  styleUrls: ['./info.page.scss'],
})
export class InfoPage implements OnInit, AfterViewInit {
  private clientInfo: AccuraScanRes;
  private jsonedDataBackInfo: string;
  constructor(private router: Router, public dataSvc: DataSvcModule, private navCtrl: NavController) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.dataSvc.prevIdentityChecked()
      .subscribe(res => {
        if (res) {
          this.dataSvc.getClientInfo()
            .subscribe(info => {
              this.clientInfo = info as AccuraScanRes;
              this.jsonedDataBackInfo = JSON.stringify(this.clientInfo.back);
            });
        } else {
          this.navCtrl.navigateForward('scan');
        }
      });
  }

  getBackScan() {
    this.router.navigateByUrl('scan');
  }
}
