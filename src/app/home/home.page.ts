import { Component, NgZone, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { DataSvcModule } from '../data-svc/data-svc.module';



@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {


  constructor(public dataSvc: DataSvcModule, private ngZone: NgZone, public navCtlr: NavController) {
  }

  ngOnInit() {
    this.ngZone.run(ngz => {
      this.dataSvc.prevIdentityChecked()
        .subscribe((identityObsv: any) => {
          if (identityObsv) {
            this.navCtlr.navigateForward("info")
          }
          else {
            this.navCtlr.navigateForward("scan")
          }
        });
    });
  }
}
