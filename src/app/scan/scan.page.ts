import { Component, NgZone, OnInit } from '@angular/core';
import { CameraResultType, Plugins } from '@capacitor/core';
const { Camera } = Plugins;
import { Document, DocumentCodeTypes, DocumentTypes } from '../data-svc/interfaces';
import { LoadingController, NavController } from '@ionic/angular';
import { DataSvcModule } from '../data-svc/data-svc.module';
import { Router } from '@angular/router';

@Component({
  selector: 'app-scan',
  templateUrl: './scan.page.html',
  styleUrls: ['./scan.page.scss'],
})
export class ScanPage implements OnInit {
  newDocument: Document;
  private documentsKind: Document[];
  private countryList;
  private selectedDocument: Document;

  constructor(private router: Router, public dataSvc: DataSvcModule, private ngZone: NgZone, private loading: LoadingController, public navCtlr: NavController) {
    this.documentsKind = [];
    this.newDocument = {} as Document;
  }

  ngOnInit() {
    this.ngZone.run(ngz => {
      this.countryList = this.dataSvc.readCountriesList()
        .subscribe(countries => {
          this.countryList = countries;
        });

      this.dataSvc.getDocumentTypes()
        .subscribe((dk: Document[]) => {
          this.documentsKind = dk;
        });
    });
  }

  async takePicture(frontSide: boolean) {
    const loading_temp: HTMLIonLoadingElement = await this.loading.create({
      message: "Loading",
      spinner: 'bubbles'
    });

    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: false,
      resultType: CameraResultType.Base64
    });

    console.log(image);
    if (frontSide) {
      this.newDocument.scannedFront = image.base64String;

      loading_temp.present();
      this.dataSvc.checkIdentity(this.newDocument);

      this.dataSvc.getClientInfo()
        .subscribe((res: any) => {
          console.log(res);
          if (res){
            if (res.status == 'Success') {
              loading_temp.dismiss();

              this.navCtlr.navigateForward('info');
            }
          }
          else{
            loading_temp.dismiss();
          }
        });
    }
    else {
      this.newDocument.scannedBack = image.base64String;

      loading_temp.present();
      this.dataSvc.checkIdentity(this.newDocument);

      this.dataSvc.getClientInfo()
        .subscribe((res: any) => {
          if (res.status == 'Success') {
            loading_temp.dismiss();

            this.navCtlr.navigateForward('info');
          }
        });
    }

  }

  selectDocument() {
    this.selectedDocument = this.documentsKind.filter(f => f.docKind === this.newDocument.key.split('-')[0] && f.docCode === this.newDocument.key.split('-')[1])[0];

    this.newDocument.docCode = this.selectedDocument.docCode;
    this.newDocument.docKind = this.selectedDocument.docKind;
  }

  
  viewInfo() {
    this.router.navigateByUrl('info');
  }
}
