import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BehaviorSubject, Observable } from 'rxjs';
import { AccuraScanRes, Document, DocumentCodeTypes, DocumentTypes } from '../data-svc/interfaces';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ]
})
export class DataSvcModule {
  private documentKind: Document[];
  private apiKey: string;
  private clientInfo: AccuraScanRes;
  private clientInfoObsv: BehaviorSubject<AccuraScanRes>;

  constructor(private httpClient: HttpClient, private http: HTTP) {
    this.clientInfoObsv = new BehaviorSubject<AccuraScanRes>({} as AccuraScanRes);
    this.documentKind = [
      {
        docKind: DocumentTypes.Passport,
        docCode: DocumentCodeTypes.Passport,
        frontAvbl: true,
        backAvbl: false
      },
      {
        docKind: DocumentTypes.IDCard,
        docCode: DocumentCodeTypes.IDCard,
        frontAvbl: true,
        backAvbl: true
      },
      {
        docKind: DocumentTypes.Visa,
        docCode: DocumentCodeTypes.Visa,
        frontAvbl: true,
        backAvbl: true
      }
    ];

    this.apiKey = "1607462232vjmXB3i8Se4gpJlHqAzx8Y7NPYexosrlf1V7HxpH";
  }

  getDocumentTypes(): Observable<Document[]> {
    return new Observable(obsv => {
      obsv.next(this.documentKind);
    });
  }

  prevIdentityChecked(): Observable<any> {
    return new Observable<any>(obsv => {
      const identity = JSON.parse(localStorage.getItem('identity_data'));

      if (identity) {
        this.clientInfoObsv.next(identity);
        obsv.next(true);
      }
      else {
        obsv.next(null);
      }
    });
  }

  readCountriesList(): Observable<any> {
    return this.httpClient.get('/assets/countries.json');
  }

  checkIdentity(document: Document) {

    const headers = new HttpHeaders();
    headers.append('Api-Key', this.apiKey);
    headers.set('User-Agent', 'postman-request');
    headers.set('Referrer-Policy', 'no-referrer');
    headers.set('Content-Type', 'multipart/form-data');

    let data = new FormData();
    data.append("country_code", document.countryCode);
    // data.append("country_code", "ESP");
    data.append("card_code", document.docCode);
    // data.append("card_code", "MRZ");

    if (document.scannedFront)
      data.append("scan_image_base64", document.scannedFront);
    else
      data.append("scan_image_base64", document.scannedBack);

    this.http.post('https://accurascan.com/api/v4/ocr', data, { headers: headers })
      .then((res: any) => {
        if (res.status == 'Success') {
          if (document.scannedFront)
            this.clientInfo.front = res;
          else
            this.clientInfo.back = res;

          this.clientInfoObsv.next(this.clientInfo);

          if (document.scannedFront)
            this.saveClientInfoData(true);
          else
            this.saveClientInfoData(false);

        }
        else {
          alert(JSON.stringify(res));
          this.clientInfoObsv.next(null);
        }
      })
      .catch(err => {
        alert(JSON.stringify(err));
        this.clientInfoObsv.next(null);
      });
  }


  saveClientInfoData(frontside: boolean) {
    let id = JSON.parse(localStorage.getItem("identity_data"));

    if (frontside) {
      id.front = this.clientInfo;
      localStorage.setItem("identity_data", JSON.stringify(id));
    }
    else {
      id.back = this.clientInfo;
      localStorage.setItem("identity_data", JSON.stringify(id));
    }
  }

  getClientInfo(): Observable<any> {
    return this.clientInfoObsv.asObservable();
  }
}
