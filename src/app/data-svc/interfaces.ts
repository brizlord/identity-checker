export interface Document {
    docKind: string;
    docCode: string;
    frontAvbl: boolean;
    backAvbl: boolean;
    scannedFront?: string;
    scannedBack?: string;
    country?: string;
    countryCode?: string;
    key?: string;
}

export enum DocumentTypes {
    Passport = "Passport",
    Visa = "Visa",
    IDCard = "ID Card"
}

export enum DocumentCodeTypes {
    Passport = "MRZ",
    Visa = "MRZ",
    IDCard = "MRZ"
}

export interface AccuraScanRes {
    front: {
        Status: string;
        Message: string;
        data: Data;
    };
    back: {
        Status: string;
        Message: string;
        data: Data;
    }
}

export interface Data {
    MRZdata: MRZdata;
}

export interface MRZdata {
    MRZ: string;
    document_type: string;
    country: string;
    last_name: string;
    first_name: string;
    document_no: string;
    document_checknumber: string;
    correct_document_checknumber: string;
    dni: string;
    nationality: string;
    date_of_birth: string;
    birth_checknumber: string;
    correct_birth_checknumber: string;
    sex: string;
    date_of_expiry: string;
    expiry_checknumber: string;
    correct_expiry_checknumber: string;
    other_id: string;
    other_id_checknumber: string;
    second_row_checknumber: string;
    correct_second_row_checknumber: string;
    flag: number;
    result: string;
    details: string;
    face_image: string;
}
